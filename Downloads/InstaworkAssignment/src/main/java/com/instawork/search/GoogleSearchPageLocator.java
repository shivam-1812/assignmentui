package com.instawork.search;

import org.openqa.selenium.By;

public interface GoogleSearchPageLocator {

    By SEARCH_FIELD = By.name("q");
    By SEARCH_BUTTON = By.name("btnK");
}
