package com.instawork.searchList;

import org.openqa.selenium.By;

public interface GoogleSearchListLocator {
    By SEARCH_LIST = By.xpath("//div[@id='search']//a[h3[contains(text(),'')]]");
    By PAGE_INDEX = By.xpath("//div[@id='navcnt']//tbody//td");
}
