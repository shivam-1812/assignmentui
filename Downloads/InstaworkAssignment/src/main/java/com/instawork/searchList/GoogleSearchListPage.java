package com.instawork.searchList;

import com.instawork.utils.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.ArrayList;
import java.util.List;

public class GoogleSearchListPage extends Helper implements GoogleSearchListLocator {
    public GoogleSearchListPage(RemoteWebDriver driver) {
        super(driver);
    }

    int currentPageIndex;
    public List<String> getAllLinks(){
        List<String> ls = new ArrayList<>();
        List<WebElement> allHeadings = finds(SEARCH_LIST);
        for (WebElement e: allHeadings){
            ls.add(e.getAttribute("href"));
        }
        return ls;
    }

    public int  findLinkIndex(List<String> links,String link){
        for(int i=0;i<links.size();i++){
            if(links.get(i).equals(link)){
                return i+1;
            }
        }
        return -1;
    }

    public void searchInNextPageNo(){
        List<WebElement> pages = finds(PAGE_INDEX);
        for(int i = 0;i<pages.size();i++){
            if(pages.get(i).getAttribute("class").equals("cur")){
                currentPageIndex = i;
            }
        }
        int next = currentPageIndex+1;  // why this next plus 1
        pages.get(next).click();
    }

}
