package com.instawork.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Helper {

    RemoteWebDriver driver;

    public Helper(RemoteWebDriver driver){
        this.driver = driver;
    }

    public WebElement find(By element){
       return new WebDriverWait(driver,10).until(ExpectedConditions.presenceOfElementLocated(element));
    }

    public List<WebElement> finds(By element){
        return driver.findElements(element);
    }
}
