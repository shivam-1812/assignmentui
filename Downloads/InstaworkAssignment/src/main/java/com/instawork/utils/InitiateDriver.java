package com.instawork.utils;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import static com.instawork.utils.PropertyReader.getProperties;


public class InitiateDriver {

    private String browser;
    private RemoteWebDriver driver;
    private String geckoDriverPath;
    private String chromeDriverPath;


    public InitiateDriver(){

        String url = getProperties().get("URL");
        browser = getProperties().get("BROWSER");
        geckoDriverPath = getProperties().get("geckoDriverPath");
        chromeDriverPath = getProperties().get("chromeDriverPath");

            try {
                driver = new RemoteWebDriver(new URL(url), getBrowserCapability());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

    }

    /*
       This method can be extended to handle different configuration
     */
    private DesiredCapabilities getBrowserCapability(){
        DesiredCapabilities capabilities = null;

        if(browser.equalsIgnoreCase("Firefox")){
            System.setProperty("webdriver.gecko.driver",geckoDriverPath);
            capabilities = DesiredCapabilities.firefox();
            capabilities.setBrowserName("firefox");
        }else if(browser.equalsIgnoreCase("Chrome")){
            System.setProperty("webdriver.chrome.driver",chromeDriverPath);
            capabilities = DesiredCapabilities.chrome();
            capabilities.setBrowserName("chrome");
        }else {
            System.out.println("Browser not supported");
        }
        return capabilities;
    }

    public RemoteWebDriver getDriver(){
        return driver;
    }
}
