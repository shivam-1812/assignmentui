package com.instawork;

import org.testng.annotations.DataProvider;

public class Data {
    @DataProvider(name = "searchTerms")
    public static Object[][] searchTerms(){

        return new Object[][]{
                {"jobs at local businesses"},
                {"instawork"}
        };
    }
}
