package com.instawork;

import com.instawork.search.GoogleSearchPage;
import com.instawork.searchList.GoogleSearchListPage;
import com.instawork.utils.InitiateDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Base64;
import java.util.List;
import java.util.Stack;

public class SearchTest {

    RemoteWebDriver driver;
    GoogleSearchPage googleSearchPage;
    GoogleSearchListPage googleSearchListPage;

    @BeforeMethod
    public void setup(){
        InitiateDriver initiateDriver = new InitiateDriver();
        driver = initiateDriver.getDriver();
        driver.get("https://www.google.com/");
        googleSearchPage = new GoogleSearchPage(driver);
        googleSearchListPage = new GoogleSearchListPage(driver);
    }

    @Test(dataProviderClass = Data.class, dataProvider = "searchTerms")
    public void verifyLinkInPage(String keyword){
        googleSearchPage.enterTextInSearch(keyword);
        constantSleep(8000);
        googleSearchPage.clickOnSearch();

        int index = -1;int i = 1;

        while (index == -1 && i<=10){
            List<String> links = googleSearchListPage.getAllLinks();
            index = googleSearchListPage.findLinkIndex(links,"https://www.instawork.com/");
            if(index==-1){
                googleSearchListPage.searchInNextPageNo();
                constantSleep(2000);
                i++;
            }
        }
        if(index != -1){
            Assert.assertTrue(index==1 && i==1,"For Keyword -> " + keyword +", Link doesn't appeared on first position on first page.It appeared on "+index+" position on page "+i);
        }

    }

    public void constantSleep(long milli){
        try {
            Thread.sleep(milli);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
